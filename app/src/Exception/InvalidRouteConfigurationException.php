<?php

namespace App\Exception;

class InvalidRouteConfigurationException extends ServerErrorException
{
    public function __construct(string $path)
    {
        parent::__construct(sprintf('Invalid route configuration for "%s".', $path));
    }
}