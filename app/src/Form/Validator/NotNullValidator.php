<?php

namespace App\Form\Validator;

class NotNullValidator implements ValidatorInterface
{
    public function validate($value): array
    {
        if ($value === null) {
            return [
                'To pole jest wymagane.'
            ];
        }

        return [];
    }

    /**
     * @return bool
     */
    public function isFormValidator(): bool
    {
        return false;
    }
}