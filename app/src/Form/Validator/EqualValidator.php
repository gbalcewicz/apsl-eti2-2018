<?php

namespace App\Form\Validator;

class EqualValidator implements ValidatorInterface
{
    /**
     * @var array
     */
    private $fields;
    /**
     * @var string
     */
    private $errorFieldName;

    public function __construct(array $fields = [], string $errorFieldName)
    {
        $this->fields = $fields;
        $this->errorFieldName = $errorFieldName;
    }

    public function validate($value): array
    {
        if (empty($this->fields)) {
            return [];
        }

        $firstValue = $value[$this->fields[0]];
        for ($i = 1; $i < count($this->fields); $i++) {
            if ($firstValue !== $value[$this->fields[$i]]) {
                return [
                    ($this->errorFieldName) => [
                        [sprintf('Wartości z pól %s się różnią', implode(', ', $this->fields))]
                    ]
                ];
            }
        }

        return [];
    }

    /**
     * @return bool
     */
    public function isFormValidator(): bool
    {
        return true;
    }
}