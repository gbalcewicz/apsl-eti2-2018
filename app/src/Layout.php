<?php

namespace App;

use App\Exception\PageNotFoundException;
use App\Session\Session;
use App\Templating\TemplatingEngine;

class Layout implements TemplatingEngine
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @param Request $request
     * @param string $page
     * @param string $name
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function render(string $page, array $params = [], string $layout = 'default'): string
    {
        extract(array_merge($params, [
            'content' => $this->renderPage($page, $params),
            'session' => $this->session
        ]));

        ob_start();
        include __DIR__ . "/../layouts/{$layout}.php";

        return ob_get_clean();
    }

    private function renderPage(string $page, array $params)
    {
        $template = __DIR__ . "/../templates/{$page}.php";

        if (!file_exists($template)) {
            throw new PageNotFoundException();
        }

        ob_start();
        extract($params);
        include $template;

        return ob_get_clean();
    }
}