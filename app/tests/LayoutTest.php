<?php

namespace Test\App;

use App\Exception\PageNotFoundException;
use App\Layout;
use App\Session\Session;
use PHPUnit\Framework\TestCase;

class LayoutTest extends TestCase
{
    private $instance;

    public function setUp(): void
    {
        $session = $this->createMock(Session::class);
        $this->instance = new Layout($session);
    }

    public function testThrowsExceptionWhenPageDoesNotExists()
    {
        $this->expectException(PageNotFoundException::class);
        $this->instance->render('nonexistenpage');
    }

    public function testUsesSpecifiedLayout()
    {
        $result = $this->instance->render('test/empty', [], 'test');

        $this->assertSame(0, strpos($result, 'TEST'));
    }

    public function testRendersPage()
    {
        $result = $this->instance->render('test/hello', [], 'test');

        $txt = <<<HTML
TEST

Hello
HTML;

        $this->assertEquals($txt, $result);
    }
}